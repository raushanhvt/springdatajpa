package com.example.springdatajpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.springdatajpa.entity.CourseMaterial;

public interface CourseMaterialRepository extends JpaRepository<CourseMaterial, Long> {

}
