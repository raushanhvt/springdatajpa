package com.example.springdatajpa.repository;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.springdatajpa.entity.Course;
import com.example.springdatajpa.entity.CourseMaterial;

@SpringBootTest
public class CourseMaterialRepositoryTest {

	@Autowired
	private CourseMaterialRepository repository;
	
	@Test
	public void saveCourseMaterial() {
		Course course = Course.builder().title(".net").credit(6).build();
		
		CourseMaterial courseMaterial = CourseMaterial.builder().url("www.dailycodebuffer.com")
				.course(course)
				.build();
		repository.save(courseMaterial);
	}
	
	@Test
	public void printAllCourseMaterial() {
	List<CourseMaterial> courseMaterials = repository.findAll();
	System.out.println("Course Mateiral:->>> "+courseMaterials);
	}
}
