package com.example.springdatajpa.repository;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import com.example.springdatajpa.entity.Course;
import com.example.springdatajpa.entity.Student;
import com.example.springdatajpa.entity.Teacher;

@SpringBootTest
public class CourseRepositoryTest {

	@Autowired
	private CourseRepository repository;

	@Test
	public void printAllCourses() {
		List<Course> courses = repository.findAll();
		System.out.println("Courses:>>>> " + courses);
	}

	@Test
	public void saveCourseWithteacher() {
		Teacher teacher = Teacher.builder().firstName("Priyanka").lastName("Singh").build();
		Course course = Course.builder().title("Python").credit(2).teacher(teacher).build();
		repository.save(course);
	}

	@Test
	public void coursepagination() {
		PageRequest firstPageWithThreeRecords = PageRequest.of(0, 3);
		PageRequest secondPageWithThreeRecords = PageRequest.of(1, 2);

		Long totalElements = repository.findAll(secondPageWithThreeRecords).getTotalElements();

		Long totalPages = (long) repository.findAll(secondPageWithThreeRecords).getTotalPages();

		List<Course> courses = repository.findAll(secondPageWithThreeRecords).getContent();
		System.out.println("Courses:>> " + courses);
		System.out.println("TotalElemetns:>> " + totalElements + "   totalPages>>> " + totalPages);

	}

	@Test
	public void findallWithSorting() {
		PageRequest sortByTitle = PageRequest.of(0, 2, Sort.by("title"));
		PageRequest sortByCreditDesc = PageRequest.of(0, 2, Sort.by("credit").descending());
		PageRequest sortByTitleAndCreditDesc = PageRequest.of(1, 2,
				Sort.by("title").descending().and(Sort.by("credit")));

		List<Course> courses = repository.findAll(sortByTitle).getContent();
		System.out.println("Sorting>>>>> " + courses);

	}

	@Test
	public void printfindbyTitleContaining() {
		PageRequest firstPageTenRecords = PageRequest.of(0, 10);
		List<Course> courses = repository.findByTitleContaining("D", firstPageTenRecords).getContent();
		System.out.println("Courses:->>>> " + courses);

	}

	@Test
	public void saveCourseStudentAndTeacher() {
		Teacher teacher = Teacher.builder().firstName("Lizze").lastName("Morgan").build();
		Student student = Student.builder().firstName("Abhishek").lastName("Singh").emailId("abhi@gmail.com").build();
		Course course = Course.builder().title("AI").credit(12).teacher(teacher).build();
		course.addStudents(student);
		repository.save(course);
	}
}
