package com.example.springdatajpa.repository;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.springdatajpa.entity.Guardian;
import com.example.springdatajpa.entity.Student;

@SpringBootTest
//@DataJpaTest
class StudentRepositoryTest {

	@Autowired
	private StudentRepository studentRepository;

	@Test
	public void saveStudent() {
		Student student = Student.builder().emailId("shabbir@gmail.com").firstName("Shabbir").lastName("Dawoodi")
				// .guardianName("Nikhil").guardianEmail("nikhil@gmail.com").guardianMobile("9999999999")
				.build();

		studentRepository.save(student);
	}

	@Test
	public void saveStudentGuardian() {
		Guardian guardian = Guardian.builder().email("nikgu@gmail.com").mobile("37383").name("nikg").build();
		Student student = Student.builder().firstName("Shivam").emailId("shivam@gmail.com").lastName("kumar")
				.guardian(guardian).build();

		studentRepository.save(student);
	}

	@Test
	public void printAllStudent() {
		List<Student> list = studentRepository.findAll();
		System.out.println("stlist:- " + list);
	}

	@Test
	public void printStudentByFirstName() {
		List<Student> list = studentRepository.findByFirstName("shivam");
		System.out.println("SHIVAM:-  " + list);
	}

	@Test
	public void printStudentByFirstNameContaining() {
		List<Student> list = studentRepository.findByFirstNameContaining("sh");
		System.out.println("SH--- :-  " + list);
	}

	@Test
	public void printStudentGuardianName() {
		List<Student> list = studentRepository.findByGuardianName("Nikhil");
		System.out.println("Guardian Name:----    " + list);

	}
	
	 @Test
	    public void printgetStudentByEmailAddress() {
	        Student student =
	                studentRepository.getStudentByEmailAddress(
	                        "shabbir@gmail.com"
	                );

	        System.out.println("student =>>:    " + student);
	    }
	
	@Test
	public void printgetStudentFirstNameByEmailAddress() {
		String firstName = studentRepository.getStudentFirstNameByEmailAddress("shabbir@gmail.com");
		System.out.println("getStudentFirstNameByEmailAddress:-->>:  "+firstName);
	}
	
	@Test
	public void printgetStudentByEmailAddressNative() {
		Student student = studentRepository.getStudentByEmailAddressNative("shabbir@gmail.com");
		System.out.println("student =>>:    " + student);
		
	}
	
	@Test
	public void printgetStudentByEmailAddressNativeNamedParam() {
		Student student = studentRepository.getStudentByEmailAddressNativeNamedParam("shabbir@gmail.com");
		System.out.println("student >:    " + student);
		
	}
	
	@Test
	public void updateStudentNameByEmailIdTest() {
		studentRepository.updateStudentNameByEmailId("ShabbirName", "shabbir@gmail.com");
	}
}
